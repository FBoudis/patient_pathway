flagTraces = function(rawData) {
  
  # Allows to extract event before and after an event of interest.
  # This permit to build the good sequence of event
  
  # input : rawData
  # output : rawData$steps :
  #   rawData$steps[[1]] = "before"
  #   rawData$steps[[2]] = "after"
  
  steps = c("before", "after")
  rawData$steps = list()
  
  data = merge(rawData$visit_detail, rawData$procedure_occurrence %>% 
                 select(visit_occurrence_id, procedure_concept_id, procedure_datetime) %>% 
                 unique(),
               by.x = "visit_occurrence_id", by.y = "visit_occurrence_id")
  
  for (step in steps) {
    
    if (step == "before") {
      
      stepData = data %>%
        mutate(flag = case_when(
          procedure_datetime > visit_detail_start_datetime ~ 1,
          procedure_datetime == visit_detail_start_datetime &
            procedure_datetime == visit_detail_end_datetime ~ 1,
          TRUE ~ 0))
      
    } else if (step == "after") {
      
      stepData = data %>%
        mutate(flag = ifelse(procedure_datetime <= visit_detail_start_datetime, 1, 0))
      
    }
    
    stepData = stepData %>%
      filter(flag == 1) %>%
      unique() %>%
      mutate(event = motif) %>%
      mutate(start = visit_detail_start_datetime) %>%
      mutate(end   = visit_detail_end_datetime) %>%
      select(visit_occurrence_id, event, start, end)
    
    assign(step, stepData)
    
    rawData$steps = append(rawData$steps, list(get(step)))
    
  }
  
  return(rawData$steps)
  
}