# OMOP patient_pathway 

*The package is still under development*

# Presentation 

Clinical pathways represents the sequence of interventions from which the patients
benefit during their encounters with health care structures. They are used in two
contexts: an individual-based context when the visualization targets a single patient to
provide appropriate care, and a population-based context when it relates to a whole
population and addresses research or decision-making objectives. There are several
complex issues which make it difficult to represent these pathways. First, the high
numbers of patients and the heterogeneity of variables increase the complexity of
datasets and induce information overload. Second, pathway data contains temporal
events that need to be aligned for population-based data analysis. Existing solutions
have some limitations. They were often developed for representing the pathway of a
selected pathology or a single patient, or they used data stored in a handmade data
model, which complicates the use of the tool in other contexts or with other database
formats.
    Observational Health Data Sciences and Informatics (OHDSI) developed the
Observational Medical Outcomes Partnership (OMOP) common data model (CDM). It
allows to standardize the data structure and vocabulary of data, which are originally
collected by different software and characterized by heterogeneous vocabularies,
depending on the country. Based on the CDM, it is possible to share tools, methods
and results. The OHDSI consortium thus shares open access R packages, ETL tools and
scripts for performing reproducible statistical analyses and evidence-based research.
    The objective of this work is to propose a tool to automate the representation of the
clinical pathways, from an individual and population points of view, based on the
OMOP CDM.

# The tools

We developed a way to analyse patient pathway with 4 types of indices. 

![Alt-Text](/images/sequenceIndices.png)

Here you have example of Sankey Diagram for the population point of view. We analyse 3 
study cases in which the steps of interest are different.

![Alt-Text](/images/SankeyDiagram.png)

